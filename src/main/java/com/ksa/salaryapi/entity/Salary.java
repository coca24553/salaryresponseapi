package com.ksa.salaryapi.entity;

import com.ksa.salaryapi.enums.PositionEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String employeeId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private PositionEnum positionEnum;

    @Column(nullable = false)
    private Double preTax;

    @Column(nullable = false)
    private Double nationalTax;

    @Column(nullable = false)
    private Double healthTax;

    @Column(nullable = false)
    private Double employmentTax;

    @Column(nullable = false)
    private Double careTax;

    @Column(nullable = false)
    private Double incomeTax;

    @Column(nullable = false)
    private Double taxFree;
}
