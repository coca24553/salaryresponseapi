package com.ksa.salaryapi.model;

import com.ksa.salaryapi.enums.PositionEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryItem {
    private Long id;
    private String employeeId;
    private String name;
    private String positionEnum;
    private Double preTax;
}
