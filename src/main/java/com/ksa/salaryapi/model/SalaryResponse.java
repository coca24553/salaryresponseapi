package com.ksa.salaryapi.model;

import com.ksa.salaryapi.enums.PositionEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryResponse {
    private Long id;
    private String employeeId;
    private String name;
    private String positionEnum;
    private Double preTax;
    private Double nationalTax;
    private Double healthTax;
    private Double employmentTax;
    private Double careTax;
    private Double incomeTax;
    private Double taxFree;
    private Double afterTax;
}
