package com.ksa.salaryapi.model;

import com.ksa.salaryapi.enums.PositionEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryRequest {
    private String employeeId;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private PositionEnum positionEnum;
    private Double preTax;
    private Double nationalTax;
    private Double healthTax;
    private Double employmentTax;
    private Double careTax;
    private Double incomeTax;
    private Double taxFree;
}
