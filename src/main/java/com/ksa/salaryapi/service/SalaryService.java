package com.ksa.salaryapi.service;

import com.ksa.salaryapi.entity.Salary;
import com.ksa.salaryapi.model.SalaryItem;
import com.ksa.salaryapi.model.SalaryRequest;
import com.ksa.salaryapi.model.SalaryResponse;
import com.ksa.salaryapi.repository.SalaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SalaryService {
    private final SalaryRepository salaryRepository;

    public void setSalary(SalaryRequest request) {
        Salary addData = new Salary();
        addData.setEmployeeId(request.getEmployeeId());
        addData.setName(request.getName());
        addData.setPositionEnum(request.getPositionEnum());
        addData.setPreTax(request.getPreTax());
        addData.setNationalTax(request.getNationalTax());
        addData.setHealthTax(request.getHealthTax());
        addData.setEmploymentTax(request.getEmploymentTax());
        addData.setCareTax(request.getCareTax());
        addData.setIncomeTax(request.getIncomeTax());
        addData.setTaxFree(request.getTaxFree());

        salaryRepository.save(addData);
    }
    public List<SalaryItem> getSalarys() {
        List<Salary> originList = salaryRepository.findAll();

        List<SalaryItem> result = new LinkedList<>();

        for (Salary salary : originList) {
            SalaryItem addItem = new SalaryItem();
            addItem.setId(salary.getId());
            addItem.setEmployeeId(salary.getEmployeeId());
            addItem.setName(salary.getName());
            addItem.setPositionEnum(salary.getPositionEnum().getName());
            addItem.setPreTax(salary.getPreTax());

            result.add(addItem);
        }
        return result;
    }

    public SalaryResponse getSalary(long id) {
        Salary originData = salaryRepository.findById(id).orElseThrow();

        SalaryResponse response = new SalaryResponse();
        response.setId(originData.getId());
        response.setEmployeeId(originData.getEmployeeId());
        response.setName(originData.getName());
        response.setPositionEnum(originData.getPositionEnum().getName());
        response.setPreTax(originData.getPreTax());
        response.setNationalTax(originData.getNationalTax());
        response.setHealthTax(originData.getHealthTax());
        response.setEmploymentTax(originData.getEmploymentTax());
        response.setCareTax(originData.getCareTax());
        response.setIncomeTax(originData.getIncomeTax());
        response.setTaxFree(originData.getTaxFree());
        response.setAfterTax(originData.getPreTax()
                -originData.getNationalTax()
                -originData.getHealthTax()
                -originData.getEmploymentTax()
                -originData.getCareTax()
                -originData.getIncomeTax()
                -originData.getTaxFree());

        return response;
    }
}
