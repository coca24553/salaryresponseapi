package com.ksa.salaryapi.controller;

import com.ksa.salaryapi.model.SalaryItem;
import com.ksa.salaryapi.model.SalaryRequest;
import com.ksa.salaryapi.model.SalaryResponse;
import com.ksa.salaryapi.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/salary")
public class SalaryController {
    private final SalaryService salaryService;

    @PostMapping("/new")
    public String setSalary(@RequestBody SalaryRequest request) {
        salaryService.setSalary(request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<SalaryItem> getSalarys() {
        return salaryService.getSalarys();
    }

    @GetMapping("/detail/{id}")
    public SalaryResponse getSalary(@PathVariable long id) {
        return salaryService.getSalary(id);
    }
}
